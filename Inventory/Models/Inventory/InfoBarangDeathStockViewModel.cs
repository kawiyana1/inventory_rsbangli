﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Inventory
{
    public class InfoBarangDeathStockViewModel
    {
        public int Id { get; set; }
        public bool Death { get; set; }
    }
}