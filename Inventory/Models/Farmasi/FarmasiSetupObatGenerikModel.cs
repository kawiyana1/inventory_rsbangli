﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Farmasi
{
    public class FarmasiSetupObatGenerikModel
    {
        public int Id { get; set; }
        public List<FarmasiSetupObatGenerikDetailModel> Detail { get; set; }
    }

    public class FarmasiSetupObatGenerikDetailModel 
    {
        public int Id { get; set; }
    }
}