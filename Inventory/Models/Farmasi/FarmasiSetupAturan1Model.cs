﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models.Farmasi
{
    public class FarmasiSetupAturan1Model
    {
        public int Id { get; set; }
        public string Jenis { get; set; }
        public string AturanPakai { get; set; }
        public int NoUrut { get; set; }
    }
}