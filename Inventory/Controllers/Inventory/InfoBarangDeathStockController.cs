﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    public class InfoBarangDeathStockController : Controller
    {
        private string tipepelayanan = "GUDANG";

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                using (var s = new SIMEntities())
                {
                    var proses = s.INV_BarangDeathStok.AsQueryable();
                    if (!string.IsNullOrEmpty(filter)) 
                    {
                        filter = filter.Trim();
                        proses = proses.Where(x =>
                            x.KOde_Barang.Contains(filter) ||
                            x.Nama_Barang.Contains(filter) ||
                            x.Nama_Kategori.Contains(filter) ||
                            x.KelompokJenis.Contains(filter));
                    }
                    var models = proses.ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Barang_ID,
                        Kode = m.KOde_Barang,
                        Nama = m.Nama_Barang,
                        Stok = m.Death_Stok ,
                        Kategori = m.Nama_Kategori,
                        Jenis = m.KelompokJenis
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Save(List<InfoBarangDeathStockViewModel> model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        if (model == null) model = new List<InfoBarangDeathStockViewModel>();
                        foreach (var x in model) 
                            s.INV_UpdateBarangDeathStok(x.Id, x.Death);

                        s.SaveChanges();
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"InfoBarangDeathStock-PROSES;".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }
    }
}