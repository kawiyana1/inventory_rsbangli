﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Inventory;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Inventory")]
    public class MutasiBarangController : Controller
    {
        private string tipepelayanan = "GUDANG";
        private string tipepelayanan2 = "FARMASI";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan
                && Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan
                && Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HConvert.Error("Bukan Tipe Pelayanan");
            if (filter.Find(x => x.Key == "Realisasi").Value == "Realisasi")
                return _ListRealisasi(orderby, orderbytype, pagesize, pageindex, filter);
            else if (filter.Find(x => x.Key == "Realisasi").Value == "Belum Semua")
                return _ListBelumSemua(orderby, orderbytype, pagesize, pageindex, filter);
            else
                return _ListMenunggu(orderby, orderbytype, pagesize, pageindex, filter);
        }

        private string _ListMenunggu(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                orderby = orderby == "No_Bukti" ? "NoBukti" :
                    orderby == "Tgl_Mutasi" ? "Tanggal" :
                    orderby == "LokasiAsal_Nama_Lokasi" ? "SectionAsal_SectionName" :
                    orderby == "LokasiTujuan_Nama_Lokasi" ? "SectionTujuan_SectionName" :
                    orderby == "Keterangan" ? "Keterangan" :
                    "";

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListGudangMutasi.Where(x => x.LokasiTujuan == lokasi && x.Batal == false).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoBukti.Contains(x.Value) ||
                                y.SectionAsal_SectionName.Contains(x.Value) ||
                                y.SectionTujuan_SectionName.Contains(x.Value) ||
                                y.Keterangan.Contains(x.Value)
                            );
                        }
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = m.NoBukti,
                        Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                        SectionAsal = m.SectionAsal_SectionName,
                        SectionTujuan = m.SectionTujuan_SectionName,
                        Keterangan = m.Keterangan,
                        Realisasi = m.Realisasi
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        private string _ListBelumSemua(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                orderby = orderby == "No_Bukti" ? "NoBukti" :
                    orderby == "Tgl_Mutasi" ? "Tanggal" :
                    orderby == "LokasiAsal_Nama_Lokasi" ? "SectionAsal_SectionName" :
                    orderby == "LokasiTujuan_Nama_Lokasi" ? "SectionTujuan_SectionName" :
                    orderby == "Keterangan" ? "Keterangan" :
                    "";

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListGudangMutasiBelumSemua.Where(x => x.LokasiTujuan == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value)) 
                        {
                            proses = proses.Where(y =>
                                y.NoBukti.Contains(x.Value) ||
                                y.SectionAsal_SectionName.Contains(x.Value) ||
                                y.SectionTujuan_SectionName.Contains(x.Value) ||
                                y.Keterangan.Contains(x.Value) 
                            );
                        }
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tanggal <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = m.NoBukti,
                        Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                        SectionAsal = m.SectionAsal_SectionName,
                        SectionTujuan = m.SectionTujuan_SectionName,
                        Keterangan = m.Keterangan,
                        Realisasi = false
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        private string _ListRealisasi(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.INV_ListGudangMutasiRealisasi.Where(x => x.LokasiAsal == lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.No_Bukti.Contains(x.Value) ||
                                y.LokasiAsal_Nama_Lokasi.Contains(x.Value) ||
                                y.LokasiTujuan_Nama_Lokasi.Contains(x.Value) ||
                                y.Keterangan.Contains(x.Value)
                            );
                        }
                        if (x.Key == "PeriodeStart" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Mutasi >= d);
                        }
                        if (x.Key == "PeriodeEnd" && !string.IsNullOrEmpty(x.Value))
                        {
                            var d = DateTime.Parse(x.Value);
                            proses = proses.Where(y => y.Tgl_Mutasi <= d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        NoBukti = m.No_Bukti,
                        Tanggal = m.Tgl_Mutasi.ToString("dd/MM/yyyy"),
                        SectionAsal = m.LokasiAsal_Nama_Lokasi,
                        SectionTujuan = m.LokasiTujuan_Nama_Lokasi,
                        Keterangan = m.Keterangan,
                        Realisasi = true
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, MutasiViewModel model)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan
                && Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HConvert.Error("Bukan Tipe Pelayanan");
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<MutasiDetailViewModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            id = s.INV_InsertMutasi(
                                model.Tanggal,
                                model.Keterangan,
                                model.MutasiDari,
                                model.NoAmprahan,
                                userid
                            ).FirstOrDefault();

                            if (string.IsNullOrEmpty(id)) throw new Exception("INV_InsertMutasi tidak mendapatkan nobukti");
                            var qtylock = 0;
                            foreach (var x in model.Detail)
                            {
                                if(x.Qty > x.QtyStok && x.Lock == "1")
                                {
                                    qtylock += 1;
                                }
                                s.INV_InsertMutasiDetail(id, x.Id, x.Qty, model.NoAmprahan, model.MutasiDari);
                            }

                            if(qtylock > 0)
                            {
                                throw new Exception("Qty Stok Tidak Mencukupi");
                            }
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Mutasi-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string DetailAmprahan(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan
                && Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    //var ket = s.
                    var m = s.INV_ListGudangAmprahanHeader.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_MutasiGetAmprahanDetail.Where(x => x.NoBukti == m.NoBukti);
                    var sectionlogin = Request.Cookies["Inventory_Section_Id"].Value;
                    var res = d.OrderBy(sectionlogin == "SEC031" ? "Kode_Barang ASC" : "Nama_Barang ASC").ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoBukti = m.NoBukti,
                            TanggalSekarang = DateTime.Today.ToString("yyyy-MM-dd"),
                            Tanggal = m.Tanggal.ToString("yyyy-MM-dd"),
                            DariSectionNama = m.SectionAsalNama,
                            LokasiID = m.SectionTujuanNama,
                            Keterangan = m.Keterangan,
                        },
                        Detail = res.ConvertAll(x => new {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Deskripsi = x.Nama_Barang,
                            Satuan = x.Satuan,
                            Kategori = x.Nama_Kategori,
                            Konversi = x.Konversi,
                            QtyTlhMutasi = x.QtyRealisasiPertama,
                            QtyStok = x.QtyStok,
                            QtyAmprah = x.Qty,
                            Harga = x.Harga,
                            KodeAkun = x.KodeAkun,
                            NamaAkunMutasi = x.NamaAkun,
                            LockStock = x.LockStock
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Detail(string id)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan
                && Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan2) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.INV_ListGudangMutasiHeader.FirstOrDefault(x => x.No_Bukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.INV_ListGudangMutasiDetail.Where(x => x.No_Bukti == m.No_Bukti);
                    var sectionlogin = Request.Cookies["Inventory_Section_Id"].Value;
                    var cek = d.OrderBy(sectionlogin == "SEC031" ? "Kode_Barang ASC" : "Nama_Barang ASC").ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoBukti = m.No_Bukti,
                            Tanggal = m.Tgl_Mutasi.ToString("yyyy-MM-dd"),
                            MutasiDari = m.SectionAsalID,
                            NoAmprahan = m.NoAmprahan,
                            TanggalAmprahan= m.TglAmprahan == null ? "" : m.TglAmprahan.Value.ToString("yyyy-MM-dd"),
                            SectionAmprahan = m.SectionTujuan,
                            Keterangan = m.Keterangan
                        },
                        Detail = cek.ConvertAll(x => new {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Deskripsi = x.Nama_Barang,
                            Konversi = x.Konversi,
                            Satuan = x.Satuan,
                            Kategori = x.Nama_Kategori,
                            Stok = x.Qty_Stok,
                            Amprahan = x.QtyAmprah,
                            Qty = x.Qty,
                            Harga = x.Harga,
                            KodeAkun = x.KodeAkun,
                            NamaAkunMutasi = x.AkunName
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}