﻿using Inventory.Entities.SIM;
using Inventory.Helper;
using Inventory.Models.Farmasi;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Farmasi
{
    public class FarmasiAnalisaStokController : Controller
    {
        private string tipepelayanan = "FARMASI";

        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HttpNotFound();
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            if (Request.Cookies["Inventory_Section_Group"].Value != tipepelayanan) return HConvert.Error("Bukan Tipe Pelayanan");
            try
            {
                if (Request.Cookies["Inventory_Section_LokasiId"] == null) throw new Exception("Lokasi Id tidak ditemukan");
                var lokasi = int.Parse(Request.Cookies["Inventory_Section_LokasiId"].Value);

                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.FAR_AnalisaStok.Where(x => x.Lokasi_ID == (short)lokasi).AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value) ||
                                y.Satuan_Stok.Contains(x.Value)
                            );
                        }
                        else if (x.Key == "MelewatiMinStok")
                        {
                            //proses = proses.Where(y => y.NamaDokter == x.Value);
                        }
                        else if (x.Key == "HariIni")
                        {
                            var d = DateTime.Today;
                            proses = proses.Where(y => y.Tanggal == d);
                        }
                        else if (x.Key == "Kemarin")
                        {
                            var d = DateTime.Today.AddDays(-1);
                            proses = proses.Where(y => y.Tanggal == d);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Kode = m.Kode_Barang,
                        Nama = m.Nama_Barang,
                        Satuan = m.Satuan_Stok,
                        QtyStok = m.Qty_Stok,
                        MinStok = m.min_Stok,
                        MaxStok = m.Max_Stok,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}